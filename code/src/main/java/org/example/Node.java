package org.example;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Node {

    public String name;
    public Map<String, String> properties = new HashMap<>();

    public Node(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return Objects.equals(name, node.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
