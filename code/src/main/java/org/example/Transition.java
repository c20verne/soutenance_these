package org.example;

public class Transition {

    public Node from;
    public Node to;
    public String type = "->";

    public Transition(Node from, Node to) {
        this.from = from;
        this.to = to;
    }

    public Transition(Node from, Node to, String type) {
        this.from = from;
        this.to = to;
        this.type = type;
    }
}
