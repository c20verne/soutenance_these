package org.example;

import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import io.gitlab.chaver.mining.patterns.constraints.factory.ConstraintFactory;
import io.gitlab.chaver.mining.patterns.io.DatReader;
import io.gitlab.chaver.mining.patterns.io.TransactionalDatabase;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.*;

public class Intro {

    public static Set<String> items = ImmutableSet.of("A", "B", "C", "D", "E");
    public static String[] itemArray = {"A", "B", "C", "D", "E"};
    public static int freqMin = 2;
    public static int k = 5;

    public static Map<String, String> getFrequentProperties() {
        Map<String, String> prop = new HashMap<>();
        prop.put("style", "filled");
        prop.put("fillcolor", "red");
        return prop;
    }

    public static Map<String, String> getClosedProperties() {
        Map<String, String> prop = new HashMap<>();
        prop.put("style", "filled");
        prop.put("fillcolor", "pink");
        return prop;
    }

    public static Map<String, String> getTopKProperties() {
        Map<String, String> prop = new HashMap<>();
        prop.put("style", "filled");
        prop.put("fillcolor", "purple");
        return prop;
    }

    public static Map<String, String> getSkyProperties() {
        Map<String, String> prop = new HashMap<>();
        prop.put("style", "filled");
        prop.put("fillcolor", "cyan");
        return prop;
    }


    public static void computeAllItemsets(TransactionalDatabase database) throws Exception {
        Model model = new Model("all");
        BoolVar[] x = model.boolVarArray("x", database.getNbItems());
        IntVar length = model.intVar("length", 1, database.getNbItems());
        model.sum(x, "=", length).post();
        IntVar freq = model.intVar("freq", 0, database.getNbTransactions());
        ConstraintFactory.coverSize(database, freq, x).post();
        Solver solver = model.getSolver();
        List<Solution> sols = solver.findAllSolutions();
        Digraph g = getDigraphFromSolutions(sols, x, new HashMap<>());
        g.properties.put("rankdir", "LR");
        String dotPath = "schemas/all.dot";
        String pdfFile = "schemas/all.pdf";
        writeGraphvizToPDF(dotPath, pdfFile, g);
    }

    public static Digraph computeFrequentItemsets(TransactionalDatabase database) throws Exception {
        Model model = new Model("frequent");
        BoolVar[] x = model.boolVarArray("x", database.getNbItems());
        IntVar length = model.intVar("length", 1, database.getNbItems());
        model.sum(x, "=", length).post();
        IntVar freq = model.intVar("freq", freqMin, database.getNbTransactions());
        ConstraintFactory.coverSize(database, freq, x).post();
        Solver solver = model.getSolver();
        List<Solution> sols = solver.findAllSolutions();
        Digraph g = getDigraphFromSolutions(sols, x, getFrequentProperties());
        g.properties.put("rankdir", "LR");
        String dotPath = "schemas/frequent.dot";
        String pdfFile = "schemas/frequent.pdf";
        writeGraphvizToPDF(dotPath, pdfFile, g);
        return g;
        /*builder = new ProcessBuilder("inkscape", "-f", "schemas/frequent.svg", "-A", "schemas/frequent.pdf");
        p = builder.start();
        exitCode = p.waitFor();*/
        //System.out.println(g);
        //solver.printStatistics();
    }

    public static int writeGraphvizToPDF(String dotPath, String pdfFile, Digraph g) throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(dotPath));
        writer.write(g.toString());
        writer.close();
        ProcessBuilder builder = new ProcessBuilder("dot", "-Tpdf", dotPath, "-o", pdfFile);
        Process p = builder.start();
        int exitCode = p.waitFor();
        return exitCode;
    }

    public static Digraph computeClosedItemsets(TransactionalDatabase database, Digraph g) throws Exception {
        Model model = new Model("closed");
        BoolVar[] x = model.boolVarArray("x", database.getNbItems());
        IntVar length = model.intVar("length", 1, database.getNbItems());
        model.sum(x, "=", length).post();
        IntVar freq = model.intVar("freq", freqMin, database.getNbTransactions());
        ConstraintFactory.coverSize(database, freq, x).post();
        ConstraintFactory.coverClosure(database, x).post();
        Solver solver = model.getSolver();
        List<Solution> sols = solver.findAllSolutions();
        transformDigraph(g, x, sols, getClosedProperties());
        //Digraph g = getDigraphFromSolutions(sols, x, getFrequentProperties());
        g.properties.put("rankdir", "LR");
        String dotPath = "schemas/closed.dot";
        String pdfFile = "schemas/closed.pdf";
        writeGraphvizToPDF(dotPath, pdfFile, g);
        return g;
        // System.out.println(g);
        //solver.printStatistics();
    }

    public static Digraph computeTopKItemsets(TransactionalDatabase database, Digraph g) throws Exception {
        Model model = new Model("top-k");
        BoolVar[] x = model.boolVarArray("x", database.getNbItems());
        IntVar length = model.intVar("length", 1, database.getNbItems());
        model.sum(x, "=", length).post();
        IntVar freq = model.intVar("freq", freqMin, database.getNbTransactions());
        ConstraintFactory.coverSize(database, freq, x).post();
        ConstraintFactory.coverClosure(database, x).post();
        Solver solver = model.getSolver();
        List<Solution> sols = solver.findAllSolutions();
        List<Integer> freqSols = new ArrayList<>();
        for (Solution sol : sols) {
            freqSols.add(sol.getIntVal(freq));
        }
        // Créez un comparateur personnalisé
        List<Solution> finalSols = sols;
        Comparator<Solution> customComparator = new Comparator<Solution>() {
            @Override
            public int compare(Solution sol1, Solution sol2) {
                int freq1 = freqSols.get(finalSols.indexOf(sol1));
                int freq2 = freqSols.get(finalSols.indexOf(sol2));
                return Integer.compare(freq2, freq1); // Pour un tri décroissant
            }
        };
        // Triez la liste 'sols' en utilisant le comparateur personnalisé
        Collections.sort(sols, customComparator);
        sols = sols.subList(0, k);
        System.out.println("Top k");
        for (Solution sol : sols) {
            System.out.println("freq=" + sol.getIntVal(freq));
        }
        transformDigraph(g, x, sols, getTopKProperties());
        g.properties.put("rankdir", "LR");
        String dotPath = "schemas/topK.dot";
        String pdfFile = "schemas/topK.pdf";
        writeGraphvizToPDF(dotPath, pdfFile, g);
        return g;
        // System.out.println(g);
        //solver.printStatistics();
    }

    public static void computeSkypatterns(TransactionalDatabase database, Digraph g) throws Exception {
        Model model = new Model("sky");
        BoolVar[] x = model.boolVarArray("x", database.getNbItems());
        IntVar length = model.intVar("length", 1, database.getNbItems());
        model.sum(x, "=", length).post();
        IntVar freq = model.intVar("freq", 1, database.getNbTransactions());
        ConstraintFactory.coverSize(database, freq, x).post();
        IntVar area = freq.mul(length).intVar();
        Solver solver = model.getSolver();
        List<Solution> sols = solver.findParetoFront(new IntVar[]{freq, area}, true);
        System.out.println("Skypatterns : ");
        for (Solution sol : sols) {
            System.out.println("freq=" + sol.getIntVal(freq) + " area=" + sol.getIntVal(area));
        }
        transformDigraph(g, x, sols, getSkyProperties());
        g.properties.put("rankdir", "LR");
        String dotPath = "schemas/sky.dot";
        String pdfFile = "schemas/sky.pdf";
        writeGraphvizToPDF(dotPath, pdfFile, g);
        // System.out.println(g);
        //solver.printStatistics();
    }

    public static void printItemsets(List<Solution> sols, BoolVar[] x) {
        for (Solution sol : sols) {
            for (int i = 0; i < x.length; i++) {
                if (sol.getIntVal(x[i]) == 1) {

                }
            }
        }
    }

    public static String convertSetToString(Set<String> set) {
        if (set.size() == 0) {
            return "{}";
        }
        StringBuilder str = new StringBuilder();
        for (String s : set) {
            str.append(s);
        }
        return str.toString();
    }

    public static String convertSolutionToItemset(Solution sol, BoolVar[] x) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < x.length; i++) {
            if (sol.getIntVal(x[i]) == 1) {
                str.append(itemArray[i]);
            }
        }
        return str.toString();
    }

    public static Digraph getDigraphFromSolutions(List<Solution> solutions, BoolVar[] x, Map<String, String> prop) {
        Digraph g = new Digraph();
        Set<Set<String>> powerSet = Sets.powerSet(items);
        List<Set<String>> powerList = new ArrayList<>();
        List<String> itemsets = new ArrayList<>();
        for (Set<String> subset : powerSet) {
            itemsets.add(convertSetToString(subset));
            powerList.add(subset);
        }
        Set<String> itemsetSols = new HashSet<>();
        for (Solution s : solutions) {
            itemsetSols.add(convertSolutionToItemset(s, x));
        }
        for (String it : itemsets) {
            Node node = new Node(it);
            if (itemsetSols.contains(it)) {
                node.properties = prop;
            }
            g.nodes.add(node);
        }
        Map<String, Node> nodeMap = new HashMap<>();
        for (int i = 0; i < itemsets.size(); i++) {
            nodeMap.put(itemsets.get(i), g.nodes.get(i));
        }
        for (int i = 0; i < powerList.size(); i++) {
            Node n = nodeMap.get(itemsets.get(i));
            for (int j = 0; j < powerList.size(); j++) {
                Node n2 = nodeMap.get(itemsets.get(j));
                if (powerList.get(j).containsAll(powerList.get(i)) && (powerList.get(j).size() - 1) == powerList.get(i).size() ) {
                    g.transitions.add(new Transition(n, n2));
                }
            }
        }
        return g;
    }

    public static void transformDigraph(Digraph g, BoolVar[] x, List<Solution> solutions, Map<String, String> prop) {
        Set<Set<String>> powerSet = Sets.powerSet(items);
        List<Set<String>> powerList = new ArrayList<>();
        List<String> itemsets = new ArrayList<>();
        for (Set<String> subset : powerSet) {
            itemsets.add(convertSetToString(subset));
            powerList.add(subset);
        }
        Set<String> itemsetSols = new HashSet<>();
        for (Solution s : solutions) {
            itemsetSols.add(convertSolutionToItemset(s, x));
        }
        for (Node n : g.nodes) {
            if (itemsetSols.contains(n.name)) {
                n.properties = prop;
            }
        }
    }

    public static void main(String[] args) throws Exception {
        TransactionalDatabase database = new DatReader("data/toy.dat").read();
        computeAllItemsets(database);
        Digraph g = computeFrequentItemsets(database);
        computeClosedItemsets(database, g);
        computeTopKItemsets(database, g);
        computeSkypatterns(database, g);
        /*computeClosedItemsets(database);
        Set<Set<String>> powerSet = Sets.powerSet(items);
        List<String> itemsets = new ArrayList<>();
        for (Set<String> subset : powerSet) {
            itemsets.add(convertSetToString(subset));
        }
        for (String it : itemsets) {
            System.out.println(it);
        }*/
    }
}
