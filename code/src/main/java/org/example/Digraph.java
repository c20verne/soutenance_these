package org.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a digraph in Graphviz
 */
public class Digraph {

    public Map<String, String> properties = new HashMap<>();

    public List<Node> nodes = new ArrayList<>();
    public List<Transition> transitions = new ArrayList<>();

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("digraph g {\n");
        for (String key : properties.keySet()) {
            str.append(key).append("=").append("\"").append(properties.get(key)).append("\"").append("\n");
        }
        for (Node node : nodes) {
            str.append("\"").append(node.name).append("\"");
            if (node.properties.size() > 0) {
                str.append("[");
                boolean first = true;
                for (String property : node.properties.keySet()) {
                    if (!first) {
                        str.append(",");
                    }
                    str.append(property).append("=\"").append(node.properties.get(property)).append("\"");
                    first = false;
                }
                str.append("]");
            }
            str.append("\n");
        }
        for (Transition t : transitions) {
            str.append("\"").append(t.from.name).append("\" ").append(t.type).append(" \"").append(t.to.name).append("\"\n");
        }
        str.append("}");
        return str.toString();
    }
}
